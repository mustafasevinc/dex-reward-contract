import {
    time,
    loadFixture,
  } from "@nomicfoundation/hardhat-toolbox/network-helpers";
  import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
  import { expectRevert } from "@nomicfoundation/hardhat-chai-matchers";
  import { expect } from "chai";
  import { ethers ,HardhatEthersSigner} from "hardhat";
import { BaseContract } from "ethers";
import { log } from "console";
  
const randomInt = (min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min;


  describe("Reward Contract", function () {
    // We define a fixture to reuse the same setup in every test.
    // We use loadFixture to run this setup once, snapshot that state,
    // and reset Hardhat Network to that snapshot in every test.
    let dexAddress: HardhatEthersSigner;
    const traders: HardhatEthersSigner[] = [];
    async function deployRewardToken(owner: HardhatEthersSigner) {

      const [ otherAccount] = await ethers.getSigners();

      const rewardTokenFactory = await ethers.getContractFactory("RewardToken");
      const rewardToken = await rewardTokenFactory.connect(owner).deploy(owner.address);

      const rewardTokenAddr = await rewardToken.getAddress();
  
      return { rewardToken, rewardTokenAddr };
    }
  
    
    async function deployReward() {
      const ONE_YEAR_IN_SECS = 365 * 24 * 60 * 60;
      const ONE_GWEI = 1_000_000_000;
      const PERIOD_DURATION = 2_592_000;
      
      const [owner, _dexAddress,trader1,trader2,trader3,trader4,] = await ethers.getSigners();
      traders.push(trader1);
      traders.push(trader2);
      traders.push(trader3);
      traders.push(trader4);

      const { rewardToken, rewardTokenAddr} = await deployRewardToken(owner);
      dexAddress=_dexAddress;
      const periodDuration = PERIOD_DURATION;
      // const unlockTime = (await time.latest()) + ONE_YEAR_IN_SECS;
  
      // Contracts are deployed using the first signer/account by default
      
      const rewardContractFactory = await ethers.getContractFactory("RewardContract");
      const rewardContract = await rewardContractFactory.deploy(dexAddress.address,rewardTokenAddr, periodDuration);
      const rewardContractAddr = await rewardContract.getAddress();
      
      const lala=await rewardToken.connect(owner).transferOwnership(rewardContract);
      await rewardContract.connect(dexAddress).mintRewardToken(rewardContractAddr,BigInt(1e36).toString());

      return { rewardToken, rewardTokenAddr, periodDuration, rewardContract ,rewardContractAddr};
    }

    // async function bulktrade(rewardContract:BaseContract):Promise<Map<string, BigInt>>{
    //   let tradeMap = new Map<string, BigInt>();

    //   const traderCount=randomInt(0,3);
    //   for (let k = 0; k < traderCount; k++) {
    //     const tradeCount=randomInt(0,10); 
    //     for (let j = 0; j < tradeCount; j++) {
    //       const traderIndex = randomInt(0,3);
    //       const tradeAmount = randomInt(1,300000)
    //       const trader = traders[traderIndex];
    //       // const tradeTransaction=await rewardContract.connect(dexAddress).registerOperation(trader.address,tradeAmount);
    //       if (tradeMap.has(trader.address)) {
    //         let lastVol=tradeMap.get(trader.address);
    //         let aaa= lastVol?.valueOf()+BigInt(tradeAmount);
    //       }else{
    //         tradeMap.set(trader.address,tradeAmount);
    //       }
    //     }
        
    //   }
    //   return tradeMap;

    // }

    describe("Register Operation", function () {
      it("Authorized Access control", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);
        
        await expect(await rewardContract.dexAddress()).to.equal(dexAddress.address);
      });

      it("Check Market Volume", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);
        
        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,100);

        await rewardContract.connect(dexAddress).registerOperation(traders[2].address,50);

        await rewardContract.connect(dexAddress).registerOperation(traders[3].address,25);

        await expect(await rewardContract.marketVolume()).to.equal(175);
      });


      it("Check Last Operation Time", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);
        
        const trade=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,100);

        await time.increase(1000);

        const trade2=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,120);

        // await rewardContract.waitForBlock(trade.blockNumber+100)
        await time.increase(1000);

        const trade3=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,120);

        let trade3Block=await ethers.provider.getBlock(trade3.blockNumber);

        await expect(await rewardContract.lastOperationTime(traders[0].address,2)).to.equal(trade3Block?.timestamp);
      });

      it("Check Trade Volume", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);
        
        const trade1=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,100);

        await time.increase(1000);

        const trade2=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,120);
        let trade2Block=await ethers.provider.getBlock(trade2.blockNumber);

        // await rewardContract.waitForBlock(trade.blockNumber+100)
        await time.increase(1000);

        const trade3=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,130);

        // Adding the "public" keyword to the traderVolume field is necessary
        await expect(await rewardContract.traderVolume(traders[0].address,trade2Block?.timestamp)).to.equal(120);
      });

      it("Next Period Starting Proccess", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);
        
        const trade1=await rewardContract.connect(dexAddress).registerOperation(traders[0].address,100);

        await time.increase(1000);

        const trade2=await rewardContract.connect(dexAddress).registerOperation(traders[2].address,120);
        let trade2Block=await ethers.provider.getBlock(trade2.blockNumber);

        // await rewardContract.waitForBlock(trade.blockNumber+100)
        await time.increase(1000);
        
        const trade3=await rewardContract.connect(dexAddress).registerOperation(traders[3].address,130);

        const previousPeriodMarketVolume=await rewardContract.marketVolume();
        const previousPeriodCount=await rewardContract.marketVolume();
        
        await time.increase(periodDuration*2);

        const trade4=await rewardContract.connect(dexAddress).registerOperation(traders[3].address,130);
        const NextPeriodMarketVolume=await rewardContract.marketVolume();


        // Adding the "public" keyword to the traderVolume field is necessary
        await expect(previousPeriodMarketVolume).to.above(NextPeriodMarketVolume);
        await expect(previousPeriodCount).to.above(await rewardContract.periodCount())
      });
    });
    
    describe("Claim Reward", function () {
      /*
      1 check period complete case
      2 check is there operation in current period?
      3 check is trade volume only for current period?
      4 reward accuracy
      5 check reward token transfer
      */

      it("check period complete case", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);
        await time.increase(periodDuration*4);
        
        await expect(rewardContract.claimRewards(traders[0].address)).to.be.revertedWith("Period has been completed. Please make a new trade");
      });

      it("check is there operation in current period", async function () {
        const { rewardTokenAddr, periodDuration, rewardContract } = await loadFixture(deployReward);

        await expect(rewardContract.claimRewards(traders[0].address)).to.be.revertedWith("No operations in current period");
      });
      
      it("check is trade volume only for current period?", async function () {
        const { rewardToken,rewardTokenAddr, periodDuration, rewardContract,rewardContractAddr } = await loadFixture(deployReward);

        const trader1trade=[BigInt(350000e18).toString(),BigInt(10000000e18).toString(),BigInt(29000000e18).toString(),BigInt(7222e18).toString(),BigInt(500000e18).toString()];

        for (let i = 0; i < trader1trade.length; i++) {
          await rewardContract.connect(dexAddress).registerOperation(traders[0].address,trader1trade[i]);
          await time.increase(2000);
        }

        await rewardContract.connect(dexAddress).registerOperation(traders[2].address,BigInt(150000e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[1].address,BigInt(46000e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[3].address,BigInt(3000e18).toString());
        await time.increase(2000);

        await rewardContract.claimRewards(traders[0].address);
        const previousPeriodReward=await rewardToken.balanceOf(traders[0].address);
        
        await time.increase(periodDuration);
        // await time.increase(2000);

        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        await time.increase(2000);
        

        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[1].address,BigInt(46000e18).toString());
        await time.increase(2000);

        await rewardContract.claimRewards(traders[0].address);
        await time.increase(2000);
        const nextPeriodReward=await rewardToken.balanceOf(traders[0].address);

        await expect(previousPeriodReward).to.above(nextPeriodReward-previousPeriodReward);
      });

      it("reward accuracy", async function () {
        const { rewardToken,rewardTokenAddr, periodDuration, rewardContract,rewardContractAddr } = await loadFixture(deployReward);

        const trader1trade=[BigInt(350000e18).toString(),BigInt(10000000e18).toString(),BigInt(29000000e18).toString(),BigInt(7222e18).toString(),BigInt(500000e18).toString()];

        for (let i = 0; i < trader1trade.length; i++) {
          await rewardContract.connect(dexAddress).registerOperation(traders[0].address,trader1trade[i]);
          await time.increase(2000);
        }

        await rewardContract.connect(dexAddress).registerOperation(traders[2].address,BigInt(150000e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[1].address,BigInt(46000e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[3].address,BigInt(3000e18).toString());
        await time.increase(2000);

        await rewardContract.claimRewards(traders[0].address);
        const previousPeriodReward=await rewardToken.balanceOf(traders[0].address);
        
        await time.increase(periodDuration);
        // await time.increase(2000);

        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        await time.increase(2000);
        

        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(350e18).toString());
        
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[1].address,BigInt(46000e18).toString());
        await time.increase(2000);

        await rewardContract.claimRewards(traders[0].address);
        await time.increase(2000);
        const nextPeriodReward=await rewardToken.balanceOf(traders[0].address);

        let trader0Vol:bigint=BigInt(150e18)+BigInt(150e18)+BigInt(350e18);
        let marketVol:bigint=BigInt(150e18)+BigInt(150e18)+BigInt(350e18)+BigInt(46000e18);
        let rewardAmount:BigInt=(trader0Vol*BigInt(1e18)/marketVol)*387n/1000n;
        let contractRewardCalc:bigint=BigInt(nextPeriodReward)-BigInt(previousPeriodReward);

        await expect(rewardAmount).to.equal(contractRewardCalc);
      });

      it("check reward token transfer", async function () {
        const { rewardToken,rewardTokenAddr, periodDuration, rewardContract,rewardContractAddr } = await loadFixture(deployReward);

        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(150e18).toString());
        await time.increase(2000);
        await rewardContract.connect(dexAddress).registerOperation(traders[0].address,BigInt(350e18).toString());
        await time.increase(2000);

        await rewardContract.connect(dexAddress).registerOperation(traders[1].address,BigInt(46000e18).toString());
        await time.increase(2000);

        await rewardContract.claimRewards(traders[0].address);
        await time.increase(2000);
        const rewardTokenBalance=await rewardToken.balanceOf(traders[0].address);

        let trader0Vol:bigint=BigInt(150e18)+BigInt(150e18)+BigInt(350e18);
        let marketVol:bigint=BigInt(150e18)+BigInt(150e18)+BigInt(350e18)+BigInt(46000e18);
        let rewardAmount:BigInt=(trader0Vol*BigInt(1e18)/marketVol)*387n/1000n;

        await expect(rewardAmount).to.equal(rewardTokenBalance);
      });
    });



  });
  
  