pragma solidity ^0.8.0;

import "./RewardToken.sol";
// import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";

contract RewardContract {
    RewardToken public rewardToken;
    uint256 public periodDuration;
    uint256 public periodCount;
    uint256 public startTime;
    address public dexAddress;
    uint256 public silincek;
    mapping(address => uint256[]) public lastOperationTime;

    mapping(address => mapping(uint256 => uint256)) public traderVolume;
    uint256 public marketVolume;
    constructor(address _dexAddr,address _rewardToken, uint256 _periodDuration) {
        rewardToken = RewardToken(_rewardToken);
        periodDuration = _periodDuration;
        periodCount = 0;
        startTime=block.timestamp;
        dexAddress=_dexAddr;
    }

    function mintRewardToken(address _to,uint256 amount) public {
        require(dexAddress == msg.sender, "Unauthorized Access");
        rewardToken.mint(_to,amount);
    }

    function registerOperation(address _trader,uint256 amount) public {
        require(dexAddress == msg.sender, "Unauthorized Access");
        uint256 nnow=block.timestamp;
        if (startTime+(periodDuration*periodCount)+periodDuration<=nnow) {
            marketVolume=0;

            periodCount+=(nnow-startTime)/periodDuration;

            // periodCount++;
        } 
        lastOperationTime[_trader].push(nnow);
        traderVolume[_trader][nnow]=amount;
        marketVolume=marketVolume+amount;               
    }
    // function resetSecondMap(address traderAddress) public {
    //     uint256 tradelenght=traderVolume[traderAddress].lenghtİ
    //     mapping(uint256 => uint256) storage secondMap = traderVolume[traderAddress];
    //     for (uint256 i = 0; i < secondMap.length; i++) {
    //         secondMap[i] = 0;
    //     }
    // }
    function claimRewards(address _trader) public {
        
        uint256[] memory operations=lastOperationTime[_trader];
        //checking
        require(startTime+(periodDuration*periodCount)+periodDuration>block.timestamp,
        "Period has been completed. Please make a new trade"
        );
        if (operations.length == 0) {
            revert("No operations in current period");
        }
        require(block.timestamp - operations[operations.length-1] < periodDuration,
        "No2 operations in current period"
        );
        // if ( fark < periodDuration) {
        //     return;
        // }
        
        //calculate trading vol in period
        uint256 tradingVol=0;
        uint256 i=operations.length;
        while (i>0) {
            if (operations[i-1]>=startTime+(periodDuration*periodCount)) {
                tradingVol+=traderVolume[_trader][operations[i-1]];
                traderVolume[_trader][operations[i-1]]=0;
                // traderVolume[_trader][operations[i-1]]=0;
                // resetSecondMap(_trader);
                // delete traderVolume[_trader][operations[i-1]];
            }
            else {
                break;
            }
            i--;
        }
        

        // for (uint256 i=operations.length-1; i==0; i--) 
        // {
        //     silincek=444;

        //     if (operations[i]>=startTime+(periodDuration*periodCount)) {
        //         tradingVol=tradingVol+traderVolume[_trader][operations[i]];
        //         silincek=tradingVol;
        //         traderVolume[_trader][operations[i]]=0;
        //     }
        //     else {
        //         break;
        //     }
        // }

        uint256 rewardAmount = calculateReward(tradingVol);
        // silincek=rewardAmount;
        // Transfer reward to trader
        rewardToken.transfer(_trader, rewardAmount);

        // Reset last operation time after successful claim
    }

    function calculateReward(uint256 tradingVolume)public view returns (uint256) {
        uint256 tradevol=mul(tradingVolume,1e18);
        // uint256 marketvol=mul(marketVolume,1e18);

        uint256 lala=div(tradevol,marketVolume);
        uint256 lolo= mul(lala,387);
        uint lili=div(lolo,1000);
        // uint lulu=mul(lili,1e26);
        return lili;





        // uint256 ratio = div(tradingVolumee,marketVolume);
        // ratio = mul(ratio,387);
        // ratio = div(ratio,1000);
        // silincek=ratio;
        // return ratio;
        
        // return ((tradingVolumee * 387) / (marketVolume * 1000));
    }


    function mul(uint256 a, uint256 b) internal pure returns (uint256 c) {
        if (a == 0) {
            return 0;
        }
        c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
     * @dev Integer division of two numbers, truncating the quotient.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        // uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return a / b;
    }

}